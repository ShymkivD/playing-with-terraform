terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "tf-infra-state-test"
    key    = "app/state.tfstate"
    region = "eu-west-3"

  }
}

provider "aws" {
  region  = "eu-west-3"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "app-vpc"
  cidr = var.vpc_cidr_block

  azs                = [var.availability_zone]
  public_subnets     = [var.subnet_cidr_block]
  public_subnet_tags = { Name = "${var.env_prefix}-subnet-1" }

  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}



module "app-webserver" {
  source = "./modules/webserver"

  env_prefix          = var.env_prefix
  availability_zone   = var.availability_zone
  vpc_id              = module.vpc.vpc_id
  instance_type       = var.instance_type
  image_name          = var.image_name
  public_key_location = var.public_key_location
  subnet_id           = module.vpc.public_subnets[0]
}
