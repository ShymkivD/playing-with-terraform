data "http" "my_ip" {
  url = "http://ifconfig.me"
}

resource "aws_security_group" "app-sg" {
  vpc_id = var.vpc_id
  name   = "app-sg"

  ingress {
    cidr_blocks = ["${data.http.my_ip.body}/32"]
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
  }

  egress {
    cidr_blocks     = ["0.0.0.0/0"]
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    prefix_list_ids = []
  }

  tags = {
    Name = "${var.env_prefix}-sg"
  }
}

data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = [var.image_name]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_key_pair" "server-key" {
  key_name   = "server-key"
  public_key = file(var.public_key_location)
}

resource "aws_instance" "app-server" {
  ami           = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type

  subnet_id                   = var.subnet_id
  vpc_security_group_ids      = [aws_security_group.app-sg.id]
  availability_zone           = var.availability_zone
  associate_public_ip_address = true

  key_name  = aws_key_pair.server-key.key_name
  user_data = file("entry-script.sh")

  tags = {
    Name = "${var.env_prefix}-ec2-server"
  }
}

