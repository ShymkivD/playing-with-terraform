variable "env_prefix" {}
variable "availability_zone" {}
variable "vpc_id" {}
variable "subnet_id" {}
variable "instance_type" {}
variable "image_name" {}
variable "public_key_location" {}
